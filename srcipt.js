
// Питання 1
// Змінну можна оголосити за допомогою ключових слів let, var або const

// Питання 2
// prompt вимагає від юзера ввести щось, alert - ні

// Питання 3
// У JS значенн можуть бути конвертовані у різні типи автоматично. Це і називають неявним перетворенням. 
// Наприклад, у 2-й задачі я використав саме його:
// let days = 4;
// console.log (days + " seconds") => значення number перетворилося на string. Це перетворення викликав оператор +, так як серед цих двох операнд є значення типу String




// Задача 1
// let name;
// let admin;
// name = "Нікіта";
// admin = name;

// Задача 2
// let days = 3;
// let seconds = days * 24 * 60 * 60;

// console.log (seconds);

// Задача 3
// const age = prompt("Введіть свій вік")
// console.log (age)